export class Slide {
    image;
    thumbnail;

    constructor(url, index) {
        this.url = url;
        this.index = index;
        this.createImgElements('image')
        this.createImgElements('thumbnail');
        this.thumbnailTransform = 0;
    }

    createImgElements(name) {
        this[name] = document.createElement('img');
        this[name].src = this.url;
    }


    calculateTransform(activeIdx) {
        const transform = (this.index - activeIdx) * 100;
        this.image.style.transform = `translate(${transform}%)`;

        this.thumbnailTransform = transform;
        this.thumbnail.style.transform = `translate(${this.thumbnailTransform}%)`;
    }

    changeThumbnails(value, parentArrayLength) {
        if (this.thumbnailTransform + value <= this.index * 100 && this.thumbnailTransform + value >= (parentArrayLength - 1 - this.index) * -100) {
            this.thumbnailTransform += value;
            this.thumbnail.style.transform = `translate(${this.thumbnailTransform}%)`;
        }
    }

}
