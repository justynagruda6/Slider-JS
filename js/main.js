import {Slider} from "./slider.class.js";

new Slider({
    images: [
        './images/bridge.jpg',
        './images/castle.jpg',
        './images/beach.jpg',
        './images/city.jpg',
        './images/island.jpg',
        './images/lake.jpg',
        './images/mountain.jpg',
        './images/tree.jpg'
    ]
})
