import {Slide} from "./slide.class.js";

export class Slider {
    constructor(config) {
        this.activeSlideIndex = 0;
        this.images = [...config.images];
        this.prevBtn = document.getElementById('prev-btn');
        this.nextBtn = document.getElementById('next-btn');
        this.prevThBtn = document.getElementById('prev-thumbnail-btn');
        this.nextThBtn = document.getElementById('next-thumbnail-btn');
        this.renderSlider();
        this.createListeners()
    }

    renderSlider() {
        this.images = this.images.map((x, i) => new Slide(x, i))
        const slideContainer = document.getElementById('slide-container');
        const thumbnailsContainer = document.getElementById('thumbnails');
        this.images.forEach(slide => {
            slide.calculateTransform(this.activeSlideIndex);
            slideContainer.appendChild(slide.image);
            thumbnailsContainer.appendChild(slide.thumbnail);
        })

    }

    changeSlidesTransform() {
        this.images.forEach(slide => {
            slide.calculateTransform(this.activeSlideIndex);
        })
    }

    changeSlide(goToSlideIdx) {
        if (goToSlideIdx < 0) {
            this.activeSlideIndex = this.images.length - 1;
        } else if (goToSlideIdx === this.images.length) {
            this.activeSlideIndex = 0;
        } else {
            this.activeSlideIndex = goToSlideIdx
        }
        this.changeSlidesTransform();
    }


    createListeners() {
        this.prevBtn.addEventListener('click', () => {
            this.changeSlide(this.activeSlideIndex - 1)
        })

        this.nextBtn.addEventListener('click', () => {
            this.changeSlide(this.activeSlideIndex + 1)
        })

        document.addEventListener('keydown', (e) => {
            if (e.key === 'ArrowLeft') {
                this.changeSlide(this.activeSlideIndex - 1)
            } else if (e.key === 'ArrowRight') {
                this.changeSlide(this.activeSlideIndex + 1)
            }
        })

        this.images.forEach((slide) => {
            slide.thumbnail.addEventListener('click', () => {
                this.changeSlide(slide.index)
            })
        })

        this.prevThBtn.addEventListener('click', () => {
            this.images.forEach(slide => {
                slide.changeThumbnails(100, this.images.length);
            })
        })

        this.nextThBtn.addEventListener('click', () => {
            this.images.forEach(slide => {
                slide.changeThumbnails(-100, this.images.length);
            })
        })
    }

}
