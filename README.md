# Slider JS
Simple slider (carousel) with thumbnails created with HTML, CSS and JavaScript. This is my first JavaScript project.<br>
Project available  [_here_](https://slider-js-just-gru.netlify.app).

## TO DO
- Create a better version for mobile devices.
- Convert into a library, which will enable the use of this slider in any project.
